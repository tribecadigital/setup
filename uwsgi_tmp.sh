PROJECT_NAME="project__name"
UWSGI=$WORKON_HOME/$PROJECT_NAME/bin/uwsgi
PIDFILE=/tmp/$PROJECT_NAME.pid
LOGFILE=/tmp/$PROJECT_NAME.log
SOCKFILE=/tmp/$PROJECT_NAME.sock

function start(){
    SCRIPT=$(readlink -f "$0")
    SCRIPTPATH=$(dirname "$SCRIPT")
    if [ -e $PIDFILE ]; then
        echo "Process already running (pid" `cat $PIDFILE`")"
    else
        $UWSGI \
            --master \
            --daemonize $LOGFILE \
            --pidfile $PIDFILE \
            --socket $SOCKFILE \
            --chdir $SCRIPTPATH \
            --wsgi-file "$PROJECT_NAME/wsgi.py" \
            --processes 4 \
            --chmod-socket=666 \
            &&
        echo "Process started successfully"
    fi
}
function stop(){
    if [ -e $PIDFILE ]; then
        #kill `cat $PIDFILE` &&
        #echo "Process" `cat $PIDFILE` "killed" &&

        $UWSGI --stop $PIDFILE &&
        rm $PIDFILE &&
        echo "Process stopped"
    else
        echo "Process not running (no pidfile found)"
    fi
}

case $1 in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        sleep 1
        start
        ;;
    *)
        echo "usage:" `basename $0` "start|stop|restart"
        ;;
esac

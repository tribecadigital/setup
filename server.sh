setupNginx(){
  clear
  echo -n "Server name: "
  read server_name
  cd /etc/nginx/sites-available/
  git clone https://gitlab.com/tribecadigital/setup.git
  mv setup/nginx.config ../sites-available/
  rm -r setup/
  sed "s|server__name|$server_name|g" nginx.config > "$server_name".config
  rm -r nginx.config
  rm -r default
  cd ../sites-enabled/
  rm -r default
  ln -s ../sites-available/"$server_name".config
  service nginx restart
  clear
  adduser $server_name
}

vimRC(){
  cd
  git clone https://gitlab.com/tribecadigital/setup.git
  mv setup/.vimrc .
  cd
  rm -r setup
}

apt-get update
apt-get upgrade
apt-get install -y git
apt-get install -y nginx
apt-get install -y vim
apt-get build-dep python-imaging
apt-get install -y virtualenvwrapper
apt-get install -y python3-dev python3-setuptools
apt-get install -y python3-pip
apt-get install -y -y gettext
apt-get install -y libfreetype6-dev
apt-get install -y python3-skimage
apt-get install -y build-essential zlib1g-dev libpcre3 libpcre3-dev unzip

vimRC
setupNginx
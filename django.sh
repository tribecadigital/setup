#!/bin/sh
# if virtualenvwrapper.sh is in your PATH (i.e. installed with pip)
source /usr/bin/virtualenv
ssh(){
  cd
  mkdir .ssh
}
djangoEnv(){
  clear
  echo -n "Env name: "
  read env_name
  mkvirtualenv --python==`which python3` $env_name
  workon $env_name
  pip install django
  pip install django-suit
  pip install uwsgi
  cd
  mkdir projects
  cd projects
  workon $env_name
  clear
}

ssh
djangoEnv